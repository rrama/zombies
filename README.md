# README #

Please note this repository's **code has not been updated since early 2013!**

This repository uses the my outdated Games API found [here](https://bitbucket.org/rrama/rramagaming).

## Content ##

In this repository is a mini-game plugin, like infection, for a game (Minecraft) with;

* In-game commands.
* Handling ingame events (e.g. players dealing damage to humans to infect them).
* A simple HUD for players with a modded client.
* A shop with purchasable items, currency is held via SQL.
* And more!

## Author ##
[rrama](https://bitbucket.org/rrama/) (Ben Durrans).