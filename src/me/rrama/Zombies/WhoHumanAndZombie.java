package me.rrama.Zombies;

import me.rrama.RramaGaming.ListToString;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class WhoHumanAndZombie implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("Zombies")) {
            if (Zombies.Dead.isEmpty() == false) {
                String PZombies = ListToString.ListToString(Zombies.Dead, ", ");
                sender.sendMessage(ChatColor.YELLOW + "Zombies: " + ChatColor.RED + PZombies);
            } else if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.InGame) {
                sender.sendMessage(ChatColor.YELLOW + "There are currently no Zombies? Let me fix that?");
                Zombies.RandomDead();
            } else if (!RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.Off) {
                sender.sendMessage(ChatColor.YELLOW + "Round has not started yet.");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Round is not in play");
            }
            return true;
        
        } else if (commandLable.equalsIgnoreCase("Humans")) {
            if (Zombies.Humans.isEmpty() == false) {
                String PHumans = ListToString.ListToString(Zombies.Humans, ", ");
                sender.sendMessage(ChatColor.YELLOW + "Humans:" + ChatColor.GREEN + PHumans);
            } else if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.InGame) {
                sender.sendMessage(ChatColor.YELLOW + "There are currently no Humans? Round should have ended...");
                Zombies.This.endRound(false);
            } else if (!RramaGaming.gameInPlay.equals("Zombies") || RramaGaming.R == RoundState.Off) {
                sender.sendMessage(ChatColor.YELLOW + "Round has not started yet.");
            } else {
                sender.sendMessage(ChatColor.YELLOW + "Round is not in play");
            }
            return true;
        } else {
            return false;
        }
    }
}