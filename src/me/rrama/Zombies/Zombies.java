package me.rrama.Zombies;

/**
 *
 * @author rrama
 * All rights reserved to rrama.
 * No copying/stealing any part of the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * No copying/stealing ideas from the code (Exceptions; You are from the Bukkit team, you have written consent from rrama).
 * 
 * Based on classic ROTM by LISTINGS09 and classic McZomg by the Derplin Dev team.
 * @Credits credit goes to rrama (author), LISTINGS09 (inspiration), the Derplin Dev team (inspiration)
 */

import java.util.ArrayList;
import java.util.Random;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

public class Zombies extends GamingPlugin {
    
    public static Random generator = new Random();
    public static ArrayList<String> Dead = new ArrayList<>();
    public static ArrayList<String> Humans = new ArrayList<>();
    public static int WinningCookies, TaggingCookies;
    public static GamingPlugin This;
    
    @Override
    public void onGameDisable() {
        Bukkit.getScheduler().cancelTasks(This);
    }
    
    @Override
    public void onGameEnable() {
        This = this;
        MinimumPlayersNeeded = 2;
        FolderSetup.FolderSetUp();
        RemoveAllMonsters();
        RegisterEvents.Register();
        
        getCommand("Zombify").setExecutor(new HumanifyAndZombify());
        getCommand("Humanify").setExecutor(new HumanifyAndZombify());
        getCommand("NewZombie").setExecutor(new NewZombie());
        getCommand("Zombies").setExecutor(new WhoHumanAndZombie());
        getCommand("Humans").setExecutor(new WhoHumanAndZombie());
        getCommand("ZombiesIntro").setExecutor(new IntroAndRules());
        getCommand("ZombiesRules").setExecutor(new IntroAndRules());
        getCommand("ZombiesTime").setExecutor(new TimedTasks.TimeCommand());
        getCommand("ZombiesAchievements").setExecutor(new AchievementCommands());
    }
    
    public static void RandomDead() {
        if (!RramaGaming.gameInPlay.equals("Zombies") || RramaGaming.R == RoundState.Off) {
            Dead.clear();
            Humans.clear();
        } else if (RramaGaming.gameInPlay.equals("Zombies") && (RramaGaming.R != RoundState.Voting) && (Humans.isEmpty())) {
            Bukkit.broadcastMessage(This.TagB +  "No body online to be a Zombie. Stopping the round.");
            This.endRound(true);
        } else if (Dead.isEmpty() == true) {
            int randomNumber = generator.nextInt(Humans.size());
            String PN = Humans.get(randomNumber);
            Dead.add(PN);
            Humans.remove(PN);
            Bukkit.broadcastMessage(This.TagNo + ChatColor.RED + PN + ChatColor.BLUE + " is now a Zombie!");
            RramaGaming.getGamer(PN).setTagColour(ChatColor.GREEN);
            for (String s : Humans) {
                RramaGaming.getGamer(s).setTagColour(ChatColor.GREEN);
            }
            for (String s : Ref.Refs) {
                RramaGaming.getGamer(s).setTagColour(ChatColor.GOLD);
            }
            for (Player P : Bukkit.getOnlinePlayers()) {
                SpoutGUI.UpdateHZLabel(P);
            }
            messageDaHZDaHZMessage();
        }
    }
    
    public static void messageDaHZDaHZMessage() {
        for (String s : Humans) {
            Player p = Bukkit.getPlayerExact(s);
            p.sendMessage(ChatColor.GOLD + "You are now a Human! Run away from Zombies.");
        }
        for (String s : Dead) {
            Player p = Bukkit.getPlayerExact(s);
            p.sendMessage(ChatColor.GOLD + "You are now a Zombie! Go hit Humans");
        }
    }
    
    @Override
    public void endRound(boolean FS) {
        ArrayList<String> NoUnhide = new ArrayList<>();
        for (Player p : Bukkit.getOnlinePlayers()) {
            NoUnhide.add(p.getName());
        }
        NoUnhide.removeAll(Ref.HiddenRefs);
        for (String s : NoUnhide) {
            Player p = Bukkit.getPlayerExact(s);
            RramaGaming.RevealPlayerToAll(p);
        }
        if (Humans.isEmpty() == true) {
            Bukkit.broadcastMessage(This.TagB +  "Round ended. " + ChatColor.RED + "Zombies " + ChatColor.BLUE + "won.");
        } else {
            String Survivors = ListToString.ListToString(Humans, ", ");
            Bukkit.broadcastMessage(This.TagB +  "Round ended. Well done to all the survivors: " + ChatColor.GREEN + Survivors);
            for (String s : Humans) {
                try {
                    RramaGaming.getGamer(s).addCookies(WinningCookies);
                    Bukkit.getPlayerExact(s).sendMessage(ChatColor.GOLD + "You got " + WinningCookies + " cookies for surviving :)");
                } catch (CookieFileException ex) {
                    ex.sendPlayerMessage();
                    ex.sendConsoleMessage();
                }
            }
            Achievements.AwardAchievement(Humans, Achievements.Achievement.Survivor);
            if (Humans.size() == 1) {
                Achievements.AwardAchievement(Humans, Achievements.Achievement.Lone_Survivor);
            }
        }
        SpoutGUI.unGUI();
        Dead.clear();
        Humans.clear();
        RemoveAllMonsters();
    }
    
    @Override
    public void startPreGame() {
        MaxHeight.BuildHeight = (int) RramaGaming.MapInPlay.getMetaData().get("Zombies-Build height");
        TimedTasks.TimerMain();
    }
    
    @Override
    public void startRound() {
        RIPTimer.TimerMain();
        for (Player p : Bukkit.getWorlds().get(0).getPlayers()) {
            RramaGaming.WasInRound.add(p.getName());
            Humans.add(p.getName());
            p.setSneaking(false);
        }
        Humans.removeAll(Ref.Refs);
        for (String s : Humans) {
            Player p = Bukkit.getPlayerExact(s);
            p.setHealth(p.getMaxHealth());
        }
        Bukkit.broadcastMessage(This.TagB +  "Round has started!");
        RandomDead();
    }
    
    public static void RemoveAllMonsters() {
        for (World w : Bukkit.getWorlds()) {
            for (Entity e : w.getEntities()) {
                if (e instanceof Monster) {
                    e.remove();
                }
            }
        }
    }
}