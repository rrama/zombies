package me.rrama.Zombies;

import java.util.HashMap;
import me.rrama.RramaGaming.Ref;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.InGameHUD;
import org.getspout.spoutapi.player.SpoutPlayer;

public class SpoutGUIDanger {
    
    public static HashMap<String, GenericLabel> HZLabels = new HashMap<>();
    
    public static void unGUI() throws ClassNotFoundException {
        for (GenericLabel GL : HZLabels.values()) {
            GL.setVisible(false);
        }
        HZLabels.clear();
    }
    
    public static void UpdateHZLabel(final Player P) throws ClassNotFoundException {
        final SpoutPlayer SP = SpoutManager.getPlayer(P);
        final InGameHUD MS = SP.getMainScreen();
        GenericLabel GL;
        if (HZLabels.containsKey(P.getName())) {
            GL = HZLabels.get(P.getName());
            if (Zombies.Humans.contains(P.getName())) {
                GL.setText(ChatColor.GREEN + "Human");
            } else if (Zombies.Dead.contains(P.getName())) {
                GL.setText(ChatColor.RED + "Zombie");
            } else if (Ref.Refs.contains(P.getName())) {
                GL.setText(ChatColor.GOLD + "Ref");
            } else {
                GL.setText(ChatColor.GRAY + "" + ChatColor.MAGIC + "***" + ChatColor.RESET + ChatColor.GRAY + "Unknown" + ChatColor.MAGIC + "***"); //Never called.
            }
        } else {
            GL = new GenericLabel();
            GL.setX(3).setY(63).setHeight(50).setWidth(100);
            HZLabels.put(P.getName(), GL);
            UpdateHZLabel(P);
            MS.attachWidget(Zombies.This, GL);
        }
    }
    
}