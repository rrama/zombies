package me.rrama.Zombies;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

public class RegisterEvents {
    
    public static void Register () {
        //PluginManager
        final PluginManager pm = Bukkit.getServer().getPluginManager();
        
        //Register Them
        pm.registerEvents(new Tagging(), Zombies.This);
        pm.registerEvents(new JoinAndQuit1(), Zombies.This);
        pm.registerEvents(new MonsterSpawn(), Zombies.This);
        pm.registerEvents(new Moon(), Zombies.This);
        pm.registerEvents(new AntiSneak(), Zombies.This);
    }
    
}
