package me.rrama.Zombies;

import me.rrama.RramaGaming.Ref;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityTargetEvent;

public class MonsterSpawn implements Listener {
    public Zombies plugin;
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.InGame && event.getEntityType() == EntityType.GIANT && event.isCancelled() == true) {
            event.setCancelled(false);
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityTarget(EntityTargetEvent event) {
        if (event.getTarget() instanceof Player) {
            Player T = (Player)event.getTarget();
            if (!RramaGaming.gameInPlay.equals("Zombies") || RramaGaming.R != RoundState.InGame) {
                event.getEntity().remove();
                event.setCancelled(true);
            } else if (Zombies.Dead.contains(T) || Ref.Refs.contains(T)) {
                event.setCancelled(true);
            }
        }
    }
}