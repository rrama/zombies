package me.rrama.Zombies;

import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.RramaGaming.Timer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TimedTasks {
    
    public static void TimerMain() {
        if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.MapReset) {
            new Timer(Zombies.This, true, 125);
        } else {
            Bukkit.broadcastMessage(Zombies.This.TagB + "TimerMain() was launched when Zombies was not in play or R != MapReset.");
        }
    }
    
    public static class TimeCommand implements CommandExecutor {

        @Override
        public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
            if (commandLable.equalsIgnoreCase("ZombiesTime") || commandLable.equalsIgnoreCase("ZTime") || commandLable.equalsIgnoreCase("ZT")) {
                if (RramaGaming.gameInPlay.equals("Zombies")) {
                    if (RramaGaming.R == RoundState.PreGame) {
                        sender.sendMessage(ChatColor.YELLOW + "Round starts in: " + ChatColor.RED + Zombies.This.timer.getTime() + " seconds.");
                    } else if (RramaGaming.R == RoundState.InGame) {
                        sender.sendMessage(ChatColor.YELLOW + "Round auto ends in: " + ChatColor.RED + Zombies.This.timer.getTime() + " seconds.");
                    } else if (RramaGaming.R == RoundState.Voting) {
                        sender.sendMessage(ChatColor.YELLOW + "Map vote is in progress.");
                    } else if (RramaGaming.R == RoundState.MapReset) {
                        sender.sendMessage(ChatColor.YELLOW + "The next map is reseting, please wait.");
                    } else if (RramaGaming.R == RoundState.AfterGame) {
                        sender.sendMessage(ChatColor.YELLOW + "The round and posibly Zombies is ending.");
                    } else if (RramaGaming.R == RoundState.NeedMorePlayers) {
                        sender.sendMessage(ChatColor.YELLOW + "Need more players before the round can begin.");
                    } else if (RramaGaming.R == RoundState.Off) {
                        sender.sendMessage(ChatColor.YELLOW + "Zombies is in play, but round is off... BREAK BREAK!");
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "Zombies is in play, but in an unknown round state.");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Zombies is not in play.");
                }
                return true;
            } else {
                return false;
            }
        }
        
    }
}