package me.rrama.Zombies;

import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinAndQuit1 implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        
        if (!RramaGaming.gameInPlay.equals("Zombies")) return;
        
        Player P = event.getPlayer();
        
        if (RramaGaming.R == RoundState.PreGame) {
            P.sendMessage(ChatColor.GOLD + "Round has not started yet. Go hide!");
        } else if (RramaGaming.R == RoundState.InGame) {
            String PN = P.getName();
            P.sendMessage(ChatColor.GOLD + "You are a Zombie! Go hit Humans.");
            if (!(Zombies.Dead.contains(PN))) {
                Zombies.Dead.add(PN);
            }
            RramaGaming.getGamer(PN).setTagColour(ChatColor.RED);
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player P = event.getPlayer();
        String PN = P.getName();

        if (Zombies.Dead.contains(PN)) {
            Zombies.Dead.remove(PN);
            if (Zombies.Dead.isEmpty() == true) {
                Bukkit.broadcastMessage(Zombies.This.TagB + "The only Zombie has left. Picking a new Zombie.");
                Zombies.RandomDead();
            }
        }

        if (Zombies.Humans.contains(PN)) {
            Zombies.Humans.remove(PN);
            if (Zombies.Humans.isEmpty() == true) {
                Bukkit.broadcastMessage(Zombies.This.TagB + "The last Human has left. Stopping the round.");
                Zombies.This.endRound(false);
            }
        }
    }
}