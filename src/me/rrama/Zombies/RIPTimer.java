package me.rrama.Zombies;

import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.RramaGaming.Timer;
import org.bukkit.Bukkit;

public class RIPTimer {
    
    public static void TimerMain() {
        if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.InGame) {
            new Timer(Zombies.This, false, 365);
        } else {
            Bukkit.broadcastMessage(Zombies.This.TagB + "TimerMain() was launched when Zombies was not in play or R != InGame.");
        }
    }
}