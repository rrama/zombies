package me.rrama.Zombies;

import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class Tagging implements Listener {
        
    public Zombies plugin;

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDamage (EntityDamageEvent event1) {
        
        Entity wounded = event1.getEntity();
        
        if (wounded instanceof Player && "Zombies".equals(RramaGaming.gameInPlay)) {
            event1.setCancelled(false);
            Player PWounded = (Player)wounded;
            String PNWounded = PWounded.getName();
            if (event1.getCause() != DamageCause.ENTITY_ATTACK) { //If the Damage is not caused by another entiry.
                if (RramaGaming.R != RoundState.InGame) {
                    event1.setCancelled(true);
                } else if (Zombies.Dead.contains(PNWounded)) {
                    event1.setCancelled(true);
                } else if (Zombies.Humans.contains(PNWounded)) {
                    if (event1.getCause() == DamageCause.LAVA || event1.getCause() == DamageCause.FIRE || event1.getCause() == DamageCause.FIRE_TICK) {
                        event1.setCancelled(true);
                    } else if (event1.getDamage() - PWounded.getHealth() >= 0) {
                        String Cause = event1.getCause().name();
                        Zombies.Humans.remove(PNWounded);
                        Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.GREEN + PNWounded + ChatColor.BLUE + " died from '" + Cause + "' and is now a Zombie.");
                        Zombies.Dead.add(PNWounded);
                        event1.setDamage(0);
                        PWounded.setHealth(PWounded.getMaxHealth());
                        RramaGaming.RevealPlayerToAll(PWounded);
                        RramaGaming.getGamer(PNWounded).setTagColour(ChatColor.RED);
                        SpoutGUI.UpdateHZLabel(PWounded);
                        if (Zombies.Humans.isEmpty() == true) {
                            Zombies.This.endRound(false);
                        }
                    }
                }
            } else {
                EntityDamageByEntityEvent event = (EntityDamageByEntityEvent)event1;
                Entity Damager = event.getDamager();
                if (!(Damager instanceof Player)) {
                    if (RramaGaming.R != RoundState.InGame) {
                        event1.setCancelled(true);
                    } else if (Zombies.Dead.contains(PNWounded)) {
                        event1.setCancelled(true);
                    } else if (Zombies.Humans.contains(PNWounded)) {
                        if (event.getDamage() - PWounded.getHealth() >= 0) {
                            String Cause = event.getCause().name();
                            Zombies.Humans.remove(PNWounded);
                            Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.GREEN + PNWounded + ChatColor.BLUE + " died from '" + Cause + "' and is now a Zombie.");
                            Zombies.Dead.add(PNWounded);
                            event.setDamage(0);
                            PWounded.setHealth(PWounded.getMaxHealth());
                            RramaGaming.getGamer(PNWounded).setTagColour(ChatColor.RED);
                            SpoutGUI.UpdateHZLabel(PWounded);
                            if (Zombies.Humans.isEmpty() == true) {
                                Zombies.This.endRound(false);
                            }
                        }
                    }
                } else {
                    Player PAttacker = (Player)Damager;
                    String PNAttacker = PAttacker.getName();
                    if (Ref.Refs.contains(PNWounded)) { //Anyone hitting a Ref.
                        PAttacker.sendMessage(ChatColor.GOLD + "Do not hit the Refs.");
                        event1.setCancelled(true);
                    } else if (RramaGaming.R != RoundState.InGame) {
                        PAttacker.sendMessage(ChatColor.GOLD + "Do not hit people untill the round has started.");
                        event1.setCancelled(true);
                    } else if (Ref.Refs.contains(PNAttacker)) { //Ref hits anyone.
                        PAttacker.sendMessage(ChatColor.GOLD + "A Ref hit you. Did you do bad?");
                    } else if ((Zombies.Dead.contains(PNWounded)) && (Zombies.Humans.contains(PNAttacker))) { //Human hits Zombie.
                        PWounded.setHealth(PWounded.getMaxHealth());
                        Achievements.AwardAchievement(PNWounded, Achievements.Achievement.Strike_Back);
                    } else if ((Zombies.Humans.contains(PNWounded)) && (Zombies.Humans.contains(PNAttacker))) { //Human hits Human.
                        PAttacker.sendMessage(ChatColor.GOLD + "Do not hit other Humans.");
                        event1.setCancelled(true);
                    } else if ((Zombies.Dead.contains(PNWounded)) && (Zombies.Dead.contains(PNAttacker))) { //Zombie hits Zombie.
                        PAttacker.sendMessage(ChatColor.GOLD + "Do not hit other Zombies.");
                        event1.setCancelled(true);
                    } else if ((Zombies.Dead.contains(PNAttacker)) && (!(Zombies.Dead.contains(PNWounded))) && (!(Zombies.Humans.contains(PNAttacker))) && (Zombies.Humans.contains(PNWounded))) { //Zombie hits Human.
                        event.setDamage(event1.getDamage()*3);
                        Achievements.AwardAchievement(PNWounded, Achievements.Achievement.Just_A_Scratch);
                        if (event.getDamage() - PWounded.getHealth() >= 0) {
                            Zombies.Humans.remove(PNWounded);
                            Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.RED + PNAttacker + ChatColor.BLUE + " ate " + ChatColor.GREEN + PNWounded + "'s " + ChatColor.BLUE + "brain.");
                            Zombies.Dead.add(PNWounded);
                            event.setDamage(0);
                            PWounded.setHealth(PWounded.getMaxHealth());
                            RramaGaming.RevealPlayerToAll(PWounded);
                            RramaGaming.getGamer(PNWounded).setTagColour(ChatColor.RED);
                            SpoutGUI.UpdateHZLabel(PWounded);
                            Achievements.AwardAchievement(PNAttacker, Achievements.Achievement.Killer);
                            Achievements.AwardAchievement(PNWounded, Achievements.Achievement.Brainz);
                            try {
                                Cookies.addCookies(PNAttacker, Zombies.TaggingCookies);
                            } catch (CookieFileException ex) {
                                ex.sendConsoleMessage();
                                ex.sendPlayerMessage();
                            }
                            if (Zombies.Humans.isEmpty()) {
                                Zombies.This.endRound(false);
                            } else if (Zombies.Humans.size() == 1) {
                                Achievements.AwardAchievement(Zombies.Humans.get(0), Achievements.Achievement.Last_Man_Standing);
                            }
                        }
                    }
                }
            }
        }
    }
}