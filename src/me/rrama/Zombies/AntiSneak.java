package me.rrama.Zombies;

import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class AntiSneak implements Listener {
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        if (RramaGaming.gameInPlay.equals("Zombies") && event.isSneaking()) event.setCancelled(true);
    }
}
