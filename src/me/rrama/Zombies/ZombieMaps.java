package me.rrama.Zombies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.RramaGaming.*;
import org.bukkit.Bukkit;

public class ZombieMaps {
    
    public static void InitializeMaps(final File F) {
        try (Scanner S = new Scanner(F)) {
            while (S.hasNextLine()) {
                String LS = S.nextLine();
                if (!LS.startsWith("#")) {
                    final String[] LSA = LS.split(" \\| ");
                    int height = Integer.parseInt(LSA[1]);
                    Zombies.This.Maps.add(LSA[0]);
                    GamingMaps.GamingMaps.get(LSA[0]).addMetaData("Zombies-Build height", height);
                }
            }
            if (Zombies.This.Maps.isEmpty()) {
                Bukkit.getLogger().warning((Zombies.This.TagB + "No mapz in da ZombiesMaps file!!! Zombies plugin die now x_x ided."));
                Bukkit.getPluginManager().disablePlugin(Zombies.This);
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((Zombies.This.TagB + "Cannot find da ZombiesMaps file!!! Zombies plugin die now x_x ided."));
            Bukkit.getPluginManager().disablePlugin(Zombies.This);
        }
    }
}