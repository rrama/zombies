package me.rrama.Zombies;

import me.rrama.RramaGaming.Ref;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HumanifyAndZombify implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {

        if (commandLable.equalsIgnoreCase("Zombify")) {
            if (!(sender.hasPermission("zombies.zombify"))) {
                sender.sendMessage(ChatColor.YELLOW + "You do not have the correct permissions.");
                return true;
            } else if (!RramaGaming.gameInPlay.equals("Zombies") || RramaGaming.R != RoundState.InGame) {
                sender.sendMessage(ChatColor.YELLOW + "Round has not started yet.");
                return true;
            } else if ((args.length == 1) && (Bukkit.getPlayer(args[0]) != null)) {
                Player P = Bukkit.getPlayer(args[0]);
                String PN = P.getName();
                if (!(Zombies.Dead.contains(PN))) {
                    if (!(Ref.Refs.contains(PN))) {
                        Zombies.Humans.remove(PN);
                        Zombies.Dead.add(PN);
                        Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.RED + PN + ChatColor.BLUE + " has been Zombified!");
                        P.sendMessage(ChatColor.GOLD + "You are now a Zombie! Go hit Humans.");
                        if (Zombies.Humans.isEmpty() == true) {
                            Bukkit.broadcastMessage(Zombies.This.TagB + "The last Human was made a Zombie. Stopping the round.");
                            Zombies.This.endRound(false);
                            return true;
                        } else {
                            RramaGaming.getGamer(PN).setTagColour(ChatColor.RED);
                            SpoutGUI.UpdateHZLabel(P);
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "You can not make a Ref a Zombie.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + PN + " is already a Zombie.");
                    return true;
                }
            } else {
                return false;
            }
        
        } else if (commandLable.equalsIgnoreCase("Humanify")) {
            if (!(sender.hasPermission("zombies.humanify"))) {
                sender.sendMessage(ChatColor.RED + "You do not have the correct permissions.");
                return true;
            } else if (!RramaGaming.gameInPlay.equals("Zombies") || RramaGaming.R != RoundState.InGame) {
                sender.sendMessage(ChatColor.YELLOW + "Round has not started yet.");
                return true;
            } else if ((args.length == 1) && (Bukkit.getPlayer(args[0]) != null)) {
                Player P = Bukkit.getPlayer(args[0]);
                String PN = P.getName();
                if (!(Zombies.Humans.contains(PN))) {
                    if (!(Ref.Refs.contains(PN))) {
                        Zombies.Dead.remove(PN);
                        Zombies.Humans.add(PN);
                        RramaGaming.RevealPlayerToAll(P);
                        Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.GREEN + PN + ChatColor.BLUE + " is Reborn! Magic!" + ChatColor.MAGIC + "FuckingHackers");
                        P.sendMessage(ChatColor.GOLD + "You are now a Human! Run away from Zombies.");
                        Achievements.AwardAchievement(PN, Achievements.Achievement.Reborn);
                        if (Zombies.Dead.isEmpty() == true) {
                            Bukkit.broadcastMessage(Zombies.This.TagB + "The only Zombie was made a Human. Picking a new zombie.");
                            Zombies.RandomDead();
                            return true;
                        } else {
                            RramaGaming.getGamer(PN).setTagColour(ChatColor.GREEN);
                            SpoutGUI.UpdateHZLabel(P);
                            return true;
                        }
                    } else {
                        sender.sendMessage(ChatColor.YELLOW + "You can not make a Ref a Human.");
                        return true;
                    }
                } else {
                    sender.sendMessage(ChatColor.GREEN + PN + ChatColor.YELLOW + " is already a Human.");
                    return true;
                }
            } else {
                return false;
            }
        
        } else {
            return false;
        }
    }
}