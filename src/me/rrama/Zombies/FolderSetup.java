package me.rrama.Zombies;

import java.io.File;
import me.rrama.Zombies.Achievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class FolderSetup {
    
    public static FileConfiguration FC;
    public static File pluginFolder, PlayersAchievementsFolder;
    
    public static void FolderSetUp() {
        FC = Zombies.This.getConfig();
        pluginFolder = Zombies.This.getDataFolder();
        if (pluginFolder.exists() == false) {
            boolean b = pluginFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((Zombies.This.TagB + "DataFolder failed to be made."));
            } else {
                Bukkit.getLogger().info((Zombies.This.TagB + "Created a DataFolder for you :). rrama do good?"));
            }
            FolderSetUp();
        } else {
            FileSetUp();
            ConfigSetUp();
        }
    }
    
    public static void FileSetUp() {
        boolean repeat = false;
        final String LSep = System.getProperty("line.separator");
        final String FS = System.getProperty("file.separator");
        File FileZombiesMaps = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "ZombiesMaps.txt", Zombies.This.TagB, "#Format: Map name | Max build height" + LSep
                + "#Example: CoolMap_YEAH | 18");
        if (FileZombiesMaps != null) {
            ZombieMaps.InitializeMaps(FileZombiesMaps);
        }

        PlayersAchievementsFolder = new File(pluginFolder, FS + "Achievements");
        if (PlayersAchievementsFolder.exists() == false) {
            boolean b = PlayersAchievementsFolder.mkdir();
            if (b == false) {
                Bukkit.getLogger().warning((Zombies.This.TagB + "Folder 'Achievements' failed to be made."));
            } else {
                Bukkit.getLogger().info((Zombies.This.TagB + "Created an Achievements folder for you :). rrama do good?"));
            }
            repeat = true;
        } else {
            for (Achievement A : Achievement.values()) {
                me.rrama.RramaGaming.FolderSetup.CreateFile(PlayersAchievementsFolder, A + ".txt", Zombies.This.TagB, null);
            }
        }

        File FileRules = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Rules.txt", Zombies.This.TagB, "No rules have been written here yet. Ask an OP for rules.");
        if (FileRules != null) {
            IntroAndRules.ReadRulesFromFile(FileRules);
        }

        File FileIntro = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "Intro.txt", Zombies.This.TagB, "No intro has been written here yet.");
        if (FileIntro != null) {
            IntroAndRules.ReadIntroFromFile(FileIntro);
        }

        File FileItemPrices = me.rrama.RramaGaming.FolderSetup.CreateFile(pluginFolder, "ItemPrices.txt", Zombies.This.TagB, "1 Block of TNT: 10" + LSep + "1 Heart: 1" + LSep + "1 Re-Birth: 50" + LSep + "1 Leather Helmet: 10" + LSep + "1 Leather Chestplate: 20" + LSep + "1 Leather Leggings: 12" + LSep + "1 Leather Boots: 10" + LSep + "1 Full Set of Leather Armour: 45" + LSep + "1 Wooden Sword: 20" + LSep + "1 Care Package: 20");
        if (FileItemPrices != null) {
            Shop.ItemPricesReg(FileItemPrices);
        }

        if (repeat) {
            FileSetUp();
        }
    }
    
    public static void ConfigSetUp() {
        if (!FC.isSet("Survivor cookies")) {
            FC.set("Survivor cookies", 10);
        }
        Zombies.WinningCookies = FC.getInt("Survivor cookies");
        
        if (!FC.isSet("Tagging cookies")) {
            FC.set("Tagging cookies", 1);
        }
        Zombies.TaggingCookies = FC.getInt("Tagging cookies");
    }
}
