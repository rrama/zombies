package me.rrama.Zombies;

import java.util.ArrayList;
import me.rrama.RramaGaming.ListToString;
import me.rrama.Zombies.Achievements.Achievement;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AchievementCommands implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {
        if (commandLable.equalsIgnoreCase("ZombiesAchievements")) {
            if (args.length == 0) {
                if (sender instanceof Player) {
                    final String PN = ((Player)sender).getName();
                    ArrayList<String> Havers = new ArrayList<>();
                    for (Achievement A : Achievement.values()) {
                        if (Achievements.PlayerHasAchievement(PN, A)) {
                            Havers.add(A.name());
                        }
                    }
                    if (Havers.isEmpty()) {
                        sender.sendMessage(ChatColor.YELLOW + "You have no Zombies achievements.");
                    } else {
                        final String All = ListToString.ListToString(Havers, ", ");
                        sender.sendMessage(ChatColor.YELLOW + "You have the Zombies achievements: " + All + ".");
                    }
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "Consoles do not get achievements, other than being the best :)");
                }
                return true;
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("All")) {
                    ArrayList<String> Allers = new ArrayList<>();
                    for (Achievement A : Achievement.values()) {
                        Allers.add(A.name());
                    }
                    final String All = ListToString.ListToString(Allers, ", ");
                    sender.sendMessage(ChatColor.YELLOW + "Avalible Zombies achievements: " + All + ".");
                } else {
                    Player P = Bukkit.getPlayer(args[0]);
                    if (P == null) {
                        sender.sendMessage(ChatColor.YELLOW + "Could not find the player spesicied.");
                    } else {
                        final String PN = P.getName();
                        ArrayList<String> Havers = new ArrayList<>();
                        for (Achievement A : Achievement.values()) {
                            if (Achievements.PlayerHasAchievement(PN, A)) {
                                Havers.add(A.name());
                            }
                        }
                        if (Havers.isEmpty()) {
                            sender.sendMessage(ChatColor.YELLOW + PN + " has no Zombies achievements.");
                        } else {
                            final String All = ListToString.ListToString(Havers, ", ");
                            sender.sendMessage(ChatColor.YELLOW + PN + " has the Zombies achievements: " + All + ".");
                        }
                    }
                }
                return true;
            } else return false;
        } else return false;
    }
}
