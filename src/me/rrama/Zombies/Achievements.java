package me.rrama.Zombies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import me.rrama.RramaGaming.ListToString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Achievements {
    
    public static boolean PlayerHasAchievement(final String PN, final Achievement A) {
        boolean AB = false;
        File AF = new File(FolderSetup.PlayersAchievementsFolder, System.getProperty("file.separator") + A + ".txt");
        try (Scanner s = new Scanner(AF)) {
            while (s.hasNextLine()) {
                if (s.nextLine().equals(PN)) AB = true;
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((Zombies.This.TagB + "Could not find File '" + AF + "'."));
        }
        return AB;
    }
    
    public static void AwardAchievement(final Player[] Ps, final Achievement A) {
        ArrayList<String> PNs = new ArrayList<>();
        for (Player p : Ps) {
            PNs.add(p.getName());
        }
        AwardAchievement(PNs, A);
    }
    
    public static void AwardAchievement(final ArrayList<String> APNs, final Achievement A) {
        ArrayList<String> PNs = new ArrayList<>();
        for (String N : APNs) {
            if (PlayerHasAchievement(N, A) == false) {
                PNs.add(N);
            }
        }
        
        if (PNs.size() > 1) {
            Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.GREEN + ListToString.ListToString(PNs, ", ") + " all earnt the achievement " + A + ".");
            for (String PN : PNs) {
                AwardAchievement(PN, A, false);
            }
        } else if (PNs.size() == 1) {
            for (String PN : PNs) {
                AwardAchievement(PN, A);
            }
        }
    }
    
    public static void AwardAchievement(final String PN, final Achievement A) {
        AwardAchievement(PN, A, true);
    }
    
    public static void AwardAchievement(final String PN, final Achievement A, final boolean B) {
        if (PlayerHasAchievement(PN, A) == false) {
            if (B) {
                Bukkit.broadcastMessage(Zombies.This.TagNo + ChatColor.GREEN + PN + " has earnt the achievement " + A + ".");
            }
            File AF = new File(FolderSetup.PlayersAchievementsFolder, System.getProperty("file.separator") + A + ".txt");
            if (AF.exists() == false) {
                Bukkit.getLogger().warning((Zombies.This.TagB + "Could not find file '" + AF + "'. " + PN + " was not added to the list."));
                Bukkit.getPlayerExact(PN).sendMessage(ChatColor.GOLD + "Something went wrong :( You did not receive the achievement " + A + ". Tell an OP.");
            } else {
                try (FileWriter FWA = new FileWriter(AF, true)) {
                    FWA.write(PN + System.getProperty("line.separator"));
                } catch (IOException ex) {
                    Bukkit.getLogger().warning((Zombies.This.TagB + "Failed to write to '" + AF + "'. " + PN + " was not added to the list."));
                    Bukkit.getPlayerExact(PN).sendMessage(ChatColor.GOLD + "Something went wrong :( You did not receive the achievement " + A + ". Tell an OP.");
                }
            }
        }
    }
    
    public static enum Achievement {
        
        /**
         * Kill a human as a zombie.
         */
        Killer,
        
        /**
         * Hit a zombie as a human.
         */
        Strike_Back,
        
        /**
         * Become a zombie, by being eaten.
         */
        Brainz,
        
        /**
         * Be the last human alive.
         */
        Last_Man_Standing,
        
        /**
         * Be hit by a zombie as a human.
         */
        Just_A_Scratch,
        
        /**
         * Be a human at the end of the round.
         */
        Survivor,
        
        /**
         * Be the only survivor.
         */
        Lone_Survivor,
        
        /**
         * Be humanified.
         */
        Reborn,
        
        /**
         * Be selected to have a real zombie spawned on you in a moon event.
         */
        Backup;
    }
}
