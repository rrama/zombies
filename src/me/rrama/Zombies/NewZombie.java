package me.rrama.Zombies;

import me.rrama.RramaGaming.Ref;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NewZombie implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLable, String[] args) {

        if (!(commandLable.equalsIgnoreCase("NewZombie"))) {
            return false;
        } else if (!(sender.hasPermission("zombies.newzombie"))) {
            sender.sendMessage(ChatColor.RED + "You do not have the correct permissions.");
            return true;
        } else if (!RramaGaming.gameInPlay.equals("Zombies") || RramaGaming.R != RoundState.InGame) {
            sender.sendMessage(ChatColor.YELLOW + "Round has not started yet.");
            return true;
        } else if (args.length == 0) {
            sender.sendMessage(ChatColor.YELLOW + "Selecting new Zombie");
            Zombies.Humans.addAll(Zombies.Dead);
            Zombies.Dead.clear();
            Zombies.RandomDead();
            return true;
        } else if (args.length == 1) {
            if (Bukkit.getPlayer(args[0]) != null) {
                Player P = Bukkit.getPlayer(args[0]);
                String PN = P.getName();
                if (!(Ref.Refs.contains(PN))) {
                    Zombies.Humans.addAll(Zombies.Dead);
                    Zombies.Dead.clear();
                    Zombies.Humans.remove(PN);
                    Zombies.Dead.add(PN);
                    sender.getServer().broadcastMessage(Zombies.This.TagNo + ChatColor.RED + PN + ChatColor.BLUE + " Is now the new Zombie.");
                    Zombies.messageDaHZDaHZMessage();
                    RramaGaming.getGamer(PN).setTagColour(ChatColor.GREEN);
                    for (String s : Zombies.Humans) {
                        RramaGaming.getGamer(s).setTagColour(ChatColor.GREEN);
                    }
                    for (Player Ps : Bukkit.getOnlinePlayers()) {
                        SpoutGUI.UpdateHZLabel(Ps);
                    }
                    return true;
                } else {
                    sender.sendMessage(ChatColor.YELLOW + "You cannot make a Ref a Zombie.");
                    return true;
                }
            } else {
                return false;
            }
        } else if (args.length > 1) {
            return false;
        } else {
            sender.sendMessage(ChatColor.YELLOW + "How the fuck is your args lenght not a length?");
            return true;
        }
    }
}