package me.rrama.Zombies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import me.rrama.RramaGaming.ShopItems.ShopCarePackage;
import me.rrama.Zombies.ShopItems.*;
import org.bukkit.Bukkit;

public class Shop {
    
    public static void ItemPricesReg(final File FileItemPrices) {
        try {
            Scanner s = new Scanner(FileItemPrices);
            s.reset();
            int count = 5;
            while (s.hasNextLine()) {
                String S = s.nextLine();
                String[] Sy = S.split(": ");
                if (Sy.length != 2) {
                    Bukkit.getLogger().warning((Zombies.This.TagB + "The line '" + S + "' does not follow the correct format for 'ItemPrices.txt'."));
                } else if (!Sy[1].matches("-?\\d+(.\\d+)?")) {
                    Bukkit.getLogger().warning((Zombies.This.TagB + "'" + Sy[1] + "' in '" + S + "' in 'ItemPrices.txt' is not an integer."));
                } else {
                    String Name = Sy[0];
                    int ItemPrice = Integer.parseInt(Sy[1]);
                    //IndiItems reg
                    if (count == 5) {
                        new TNT(Zombies.This, Name, ItemPrice);
                    } else if (count == 6) {
                        new Cookie(Zombies.This, Name, ItemPrice);
                    } else if (count == 7) {
                        new ReBirth(Zombies.This, Name, ItemPrice);
                    } else if (count == 8) {
                        new LeatherHelmet(Zombies.This, Name, ItemPrice);
                    } else if (count == 9) {
                        new LeatherChestplate(Zombies.This, Name, ItemPrice);
                    } else if (count == 10) {
                        new LeatherLeggings(Zombies.This, Name, ItemPrice);
                    } else if (count == 11) {
                        new LeatherBoots(Zombies.This, Name, ItemPrice);
                    } else if (count == 12) {
                        new LeatherFull(Zombies.This, Name, ItemPrice);
                    } else if (count == 13) {
                        new WoodenSword(Zombies.This, Name, ItemPrice);
                    } else if (count == 14) {
                        new ShopCarePackage(Zombies.This, Name, ItemPrice);
                    }
                    //IndiItems reg end
                }
                count++;
            }
        } catch (FileNotFoundException ex) {
            Bukkit.getLogger().warning((Zombies.This.TagB + "Could not find File 'ItemPrices.txt' even though it exists."));
        }
    }
}