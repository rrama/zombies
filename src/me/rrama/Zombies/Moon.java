package me.rrama.Zombies;

import java.util.ArrayList;
import me.rrama.RramaGaming.MoonPhaseChangeEvent;
import me.rrama.RramaGaming.MoonPhaseChangeEvent.MoonPhase;
import me.rrama.RramaGaming.RoundState;
import me.rrama.RramaGaming.RramaGaming;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class Moon implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onMoonPhaseChange(MoonPhaseChangeEvent event) {
        MoonPhase MP = event.getMoonPhase();
        if (MP == MoonPhase.FullMoon) {
            Bukkit.broadcastMessage(Zombies.This.TagB + "The Full Moon is upon you.");
            if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.InGame) {
                Bukkit.broadcastMessage(Zombies.This.TagB +  "The undead have awaken!");
                ArrayList<String> Names = new ArrayList<>();
                for (int i = 0; i <= 5; i++) {
                    String PN = Zombies.Dead.get(Zombies.generator.nextInt(Zombies.Dead.size()));
                    Player P = Bukkit.getPlayerExact(PN);
                    P.getWorld().spawnEntity(P.getLocation(), EntityType.ZOMBIE);
                    if (!Names.contains(PN)) {
                        Names.add(PN);
                    }
                }
                Achievements.AwardAchievement(Names, Achievements.Achievement.Killer);
            }
        } else if (MP == MoonPhase.NewMoon) {
            Bukkit.broadcastMessage(Zombies.This.TagB + "No Moon shines in the night sky tonight.");
            if (RramaGaming.gameInPlay.equals("Zombies") && RramaGaming.R == RoundState.InGame) {
                Bukkit.broadcastMessage(Zombies.This.TagB + "The Zombies... I can't see them... They're invisible! Run!");
                HideZombies();
                LightningAndHiding();
            }
        } else if (MP == MoonPhase.WaxingCrescent) {
            UnHideZombies();
        }
    }
    
    public static void HideZombies() {
        for (String s : Zombies.Humans) {
            Player p = Bukkit.getPlayerExact(s);
            for (String sh : Zombies.Dead) {
                Player ph = Bukkit.getPlayerExact(sh);
                p.hidePlayer(ph);
            }
        }
    }
    
    public static void UnHideZombies() {
        for (String s : Zombies.Dead) {
            Player p = Bukkit.getPlayerExact(s);
            RramaGaming.RevealPlayerToAll(p);
        }
    }
    
    public static void LightningAndHiding() {
        for (String s : Zombies.Dead) {
            Player p = Bukkit.getPlayerExact(s);
            World w = p.getWorld();
            Location l = p.getLocation();
            w.strikeLightningEffect(l);
            for (Player P : Bukkit.getWorlds().get(0).getPlayers()) {
                P.sendBlockChange(l, l.getBlock().getTypeId(), l.getBlock().getData());
            }
        }
        UnHideZombies();
        Bukkit.getScheduler().runTaskLater(Zombies.This, new Runnable() {
            @Override
            public void run() {
                HideZombies();
            }
        }, 3*20);
    }
}
