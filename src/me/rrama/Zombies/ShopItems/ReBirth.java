package me.rrama.Zombies.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import me.rrama.Zombies.Zombies;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ReBirth extends ShopItem {
    
    public ReBirth(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean isVisibleTo(final Player P) {
        return Zombies.Dead.contains(P.getName());
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (!Zombies.Dead.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "You are not dead so can't be re-born. Logicks.");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        return Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "Humanify " + P.getName());
    }
    
}