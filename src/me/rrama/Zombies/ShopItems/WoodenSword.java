package me.rrama.Zombies.ShopItems;

import me.rrama.RramaGaming.Gamer;
import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.RramaGaming;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class WoodenSword extends ShopItem {
    
    public WoodenSword(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean isVisibleTo(final Player P) {
        Gamer G = RramaGaming.getGamer(P.getName());
        return G.m();
    }
    
    @Override
    public boolean canBuy(final Player P) {
        Gamer G = RramaGaming.getGamer(P.getName());
        if (!G.m()) {
            P.sendMessage(ChatColor.YELLOW + "You must be playing to buy "+getName()+".");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        ItemStack IS = new ItemStack(Material.WOOD_SWORD, 1);
        return P.getInventory().addItem(IS).isEmpty();
    }
    
}