package me.rrama.Zombies.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import me.rrama.Zombies.Zombies;
import org.bukkit.entity.Player;

public class LeatherFull extends ShopItem {
    
    public LeatherFull(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean perform(final Player P) {
        boolean b = true;
        for (int i = 1; i < 5; ++i) {
            if (!Zombies.This.ShopItems.get(i).perform(P)) {
                b = false;
            }
        }
        return b;
    }
    
}