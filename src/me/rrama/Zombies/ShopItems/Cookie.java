package me.rrama.Zombies.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import me.rrama.Zombies.Zombies;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Cookie extends ShopItem {
    
    public Cookie(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean isVisibleTo(final Player P) {
        return Zombies.Humans.contains(P.getName());
    }
    
    @Override
    public boolean canBuy(final Player P) {
        if (!Zombies.Humans.contains(P.getName())) {
            P.sendMessage(ChatColor.YELLOW + "Only humans find cookies ummy.");
            return false;
        }
        return true;
    }
    
    @Override
    public boolean perform(final Player P) {
        ItemStack IS = new ItemStack(Material.COOKIE, 1);
        return P.getInventory().addItem(IS).isEmpty();
    }
    
}