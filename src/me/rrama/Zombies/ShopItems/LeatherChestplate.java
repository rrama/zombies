package me.rrama.Zombies.ShopItems;

import me.rrama.RramaGaming.GamingPlugin;
import me.rrama.RramaGaming.ShopItems.ShopItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LeatherChestplate extends ShopItem {
    
    public LeatherChestplate(final GamingPlugin GP, final String Name, final int Price) {
        super(GP, Name, Price);
    }
    
    @Override
    public boolean perform(final Player P) {
        ItemStack IS = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        return P.getInventory().addItem(IS).isEmpty();
    }
    
}